#include <TM1637.h>
#include <DS3231.h>

// Instantiation and pins configurations
// Pin 2 - > DIO
// Pin 3 - > CLK
TM1637 tm1637(2, 3);
DS3231 Clock;

#define ADRESSE_I2C_RTC 0x68
byte decToBcd(byte val){return( (val/10*16) + (val%10));}
byte bcdToDec(byte val){return( (val/16*10) + (val%16));}



void setup()
{
    tm1637.init();
    tm1637.setBrightness(1);
  Wire.begin();
  Wire.beginTransmission(ADRESSE_I2C_RTC);
  /*Clock.setYear(2020);
  Clock.setMonth(11);
  Clock.setDate(28);
  Clock.setDoW(6);
  Clock.setHour(7);
  Clock.setMinute(23);
  Clock.setSecond(0);*/
}

byte seconde, minute, heure, numJourSemaine, numJourMois, mois, annee;

void loop()
{
    Wire.beginTransmission(ADRESSE_I2C_RTC);
Wire.write(0); // Positionner le pointeur de registre sur 00h
Wire.endTransmission();
Wire.requestFrom(ADRESSE_I2C_RTC, 7);

seconde = bcdToDec(Wire.read() & 0x7f);
minute = bcdToDec(Wire.read());
heure = bcdToDec(Wire.read() & 0x3f);
numJourSemaine = bcdToDec(Wire.read());
numJourMois = bcdToDec(Wire.read());
mois = bcdToDec(Wire.read());
annee = bcdToDec(Wire.read());

            tm1637.dispNumber(minute + heure * 100);
            tm1637.switchColon();
delay(1000);
            


}
