#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

#ifndef PSTR
   #define PSTR // Make Arduino Due happy  ||| I don't know why
#endif

#define PIN 3 /// connect on D3



#define HEIGHT 8
#define WIDTH 32
#define TIMEWAIT 50
#define WAVESIZE 32


Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(WIDTH, HEIGHT, PIN,
  NEO_MATRIX_BOTTOM    + NEO_MATRIX_RIGHT +
  NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG ,     /// ZIGZAG for matrix display 
  NEO_GRB            + NEO_KHZ800);

void setup() {
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(10);
  matrix.clear();
   matrix.show();
// Init Sinus Table
  tsin();
  Serial.begin(9600);

}


int dt=1;
int t=0;

float tabsin[WAVESIZE];


void tsin()
{
for (char x=0;x<WAVESIZE;x++)
  {
    tabsin[x]=sin(x*2*3.1415926/WAVESIZE);
    
  }
}

void loop() {

t=t+dt;
if (t<0 || t>200) { dt*=-1; }

for (char y=0;y<HEIGHT;y++)
for (char x=0;x<WIDTH;x++)
{

int color = (int) 
  (
     128.0 + (128.0 * tabsin[(t+x)%   WAVESIZE])
   + 128.0 + (128.0 * tabsin[(t+y)%   WAVESIZE])
   + 128.0 + (128.0 * tabsin[(x+y+t)% WAVESIZE])
   + 128.0 + (128.0 * tabsin[ (int)(sqrt( double( ( (x+t)*(x+t) + (y+0.5*t)*(y+0.5*t) ) / 2.0))) % WAVESIZE])
 //  + 128.0 + (128.0 *  sin( sqrt( double((x-32/2.0)* (x - 32 / 2.0) + (y - 8 / 2.0) * (y - 8 / 2.0))) / 3.0))
 // + 128.0 + (128.0 * sin( sqrt(double((x+t) * (x+t) + y * y)) / 2.0))
  )/4.0;

// Get color from rainbow from 0 to 255
  matrix.drawPixel(x,y,(uint32_t)Wheel((color)%256)  ) ;
}
matrix.show();
delay(TIMEWAIT);
  
}


uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
   return matrix.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if(WheelPos < 170) {
    WheelPos -= 85;
   return matrix.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  } else {
   WheelPos -= 170;
   return matrix.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}
